package com.example.connect3game;

import androidx.appcompat.app.AppCompatActivity;
import androidx.gridlayout.widget.GridLayout;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    // 1 : cross , 0 : circle , 2 : empty
    //thired branch :)))))))
    int activeplayer = 0;
    int[] gamestate = {2,2,2,2,2,2,2,2,2};  //positions where tapped
      int[][] winningstate = {{0,1,2},{3,4,5},{6,7,8},{0,3,6},{1,4,7},{2,5,8},{0,4,8},{2,4,6},{6,4,2},{8,4,0}};
      Boolean gameactive = true;
      public void dropin(View view)
        {
            ImageView counter = (ImageView) view;
            int tappedcounter = Integer.parseInt(counter.getTag().toString());  // tapped on any view(0-8) used to identify tapped on which view
            if (gamestate[tappedcounter] == 2 && gameactive == true)
            {
                counter.setTranslationY(-1000);
                gamestate[tappedcounter] = activeplayer;  // get which player tapped and store to gamestate
                if (activeplayer == 0) {
                    counter.setImageResource(R.drawable.cross);
                    activeplayer = 1;
                } else {
                    counter.setImageResource(R.drawable.circle2);
                    activeplayer = 0;
                }
                counter.animate().translationYBy(1000).setDuration(500);

                for (int i[] : winningstate) {
                    String message = "";
                    if (gamestate[i[0]] == gamestate[i[1]] && gamestate[i[1]] == gamestate[i[2]] && gamestate[i[0]] != 2) {
                        if (activeplayer == 1)
                            message = "Cross";
                        else

                            message = "Circle";
                        gameactive = false;
                        Toast.makeText(this, "winner is " + message, Toast.LENGTH_SHORT).show();
                        Button playagain = (Button) findViewById(R.id.button);
                        TextView winner = (TextView) findViewById(R.id.textView);
                        winner.setText("winner is " + message);
                        playagain.setVisibility(View.VISIBLE);
                        winner.setVisibility(View.VISIBLE);
                    }
                }
        }
 }
              public void playagain(View v)
              {     Button playagain = (Button) findViewById(R.id.button);
                  TextView winner = (TextView) findViewById(R.id.textView);
                  playagain.setVisibility(View.INVISIBLE);
                  winner.setVisibility(View.INVISIBLE);
                  Log.i("values","play again");
                  GridLayout gridLayout = (GridLayout) findViewById(R.id.grid);
                  for(int i=0; i<gridLayout.getChildCount();i++)
                  {
                      ImageView image = (ImageView) gridLayout.getChildAt(i);
                      image.setImageDrawable(null);
                  }

                  for(int i=0; i<gamestate.length;i++)
                  {
                      gamestate[i] =2;
                  }
                  gameactive= true;
                  activeplayer = 1;
              }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
